package com.pibigstar.config;

import java.util.List;

/**
 * <mapper namespace="com.pibigstar.mapper.UserMapper">
 *      <select id="getUserById" resultType ="com.pibigstar.bean.User">
 *          select * from user where id = ?
 *      </select>
 * </mapper>
 *
 * @author pibigstar
 */

public class MapperBean {

    //接口的名字  com.pibigstar.mapper.UserMapper
    private String interfaceName;

    //接口下面的所有方法 getUserById
    private List<Function> functions;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }


}
