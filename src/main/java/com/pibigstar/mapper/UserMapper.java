package com.pibigstar.mapper;

import com.pibigstar.bean.User;

public interface UserMapper {
	
	User getUserById(String id);

}
