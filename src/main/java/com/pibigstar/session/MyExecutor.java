package com.pibigstar.session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.pibigstar.bean.User;

/**
 * MyExecutor中封装了JDBC的操作：
 *
 * @author pibigstar
 */
public class MyExecutor implements Executor {

    private MyConfiguration config = new MyConfiguration();
    private PreparedStatement ps = null;
    private Connection connection = null;
    private ResultSet rs = null;

    @Override
    public <T> T query(String sql, Object parameter) {
        connection = getConnection();
        rs = null;

        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, parameter.toString());

            rs = ps.executeQuery();
            User user = new User();
            while (rs.next()) {
                user.setId(rs.getString("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
            }
            return (T) user;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return null;
    }

    @Override
    public int delete(String sql, Object parameter) {
        connection = getConnection();
        ps = null;
        int result = 0;
        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(1, parameter);

            int[] ints = ps.executeBatch();
            result = ints.length;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return result;
    }

    @Override
    public void add(String sql, Object parameter) {
        Connection connection = getConnection();

        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(1, parameter);

            ps.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    @Override
    public int update(String sql, Object parameter) {
        Connection connection = getConnection();
        int result = 0;
        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(1, parameter);

            result = ps.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return result;
    }

    private Connection getConnection() {
        connection = config.build("config.xml");
        return connection;
    }

    private void close() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
