package com.pibigstar.session;

import java.lang.reflect.Proxy;

import com.pibigstar.proxy.MyMapperProxy;

/**
 * 接下来实现我们的MySqlSession,
 * 首先的成员变量里得有Executor和MyConfiguration，代码的精髓就在getMapper的方法里。
 * @author pibigstar
 *
 */
public class MySqlSession {
	
	private Executor executor = new MyExecutor();
	
	private MyConfiguration myConfiguration = new MyConfiguration();
	
	public <T> T selectOne(String sql,String parameter) {

		return executor.query(sql, parameter);
	}
	
	/**
	 * 调用代理
	 * @param clazz
	 * @return
	 */
	public <T> T getMapper(Class clazz) {
		
		return (T)Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] {clazz}, new MyMapperProxy(this, myConfiguration));
		
	}
	
	

}
