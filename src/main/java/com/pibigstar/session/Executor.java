package com.pibigstar.session;

/**
 * 查询接口
 * @author pibigstar
 *
 */
public interface Executor {
	
	<T> T query(String sql, Object parameter);

	int delete(String sql,Object parameter);

	void add(String sql,Object parameter);

	int update(String sql,Object parameter);

}
